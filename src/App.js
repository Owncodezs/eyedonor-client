import logo from './logo.svg';
import './App.css';
import RegisterForm from './components/RegisterForm';
import RegisterTable from './components/RegisterTable';

function App() {
  return (
    <div>
    <RegisterForm/>
    <RegisterTable/>
    </div>
  );
}

export default App;
